#!/usr/bin/env bash
##----------------------------------------------------------------------------##
##                       __      __                  __   __                  ##
##               .-----.|  |_.--|  |.--------.---.-.|  |_|  |_                ##
##               |__ --||   _|  _  ||        |  _  ||   _|   _|               ##
##               |_____||____|_____||__|__|__|___._||____|____|               ##
##                                                                            ##
##  File      : sanitize.sh                                                   ##
##  Project   : _fonts_                                                       ##
##  Date      : Sep 27, 2019                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2019                                                ##
##                                                                            ##
##  Description :                                                             ##
##    The tool doesn't like the .TTF extension...                             ##
##    So this script just converts them to .ttf!                              ##
##----------------------------------------------------------------------------##

##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
find . -name "*.TTF" > list.txt
while read FULLPATH; do
    DIRNAME=$(dirname "$FULLPATH");
    FILENAME=$(basename "$FULLPATH");
    FILENAME_NO_EXT=$(basename "$FULLPATH" ".TTF");
    NEW_FILENAME="${FILENAME_NO_EXT}.ttf";

    echo "Dirname      : ($DIRNAME)";
    echo "Filename     : ($FILENAME)";
    echo "New Filename : ($NEW_FILENAME)";

    mv -v "$FULLPATH" "${DIRNAME}/${NEW_FILENAME}";
    echo "";
done < list.txt
